    /**
	 *  redis锁的错误使用
     *  redis是单线程的
     *  setIfAbsent设置值。如果值已存在返回fasle
     *  getAndSet设置值并且返回原来的值
     *  分布式锁实现，为防止解锁失败值加上时效
     * @return 是否获取成功
     */
    public Boolean lock(){
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        if(redisTemplate.opsForValue().setIfAbsent("key","带时间戳的值")){
            return true;
        }
        //超时
        String old = redisTemplate.opsForValue().get("key");
        if(old!=null&&Long.parseLong(old)>System.currentTimeMillis()){
            //超时两个线程都拿到了旧的值，直接set的话两个都拿到锁
            //通过getAndSet，返回旧的值设置新的值，第一个getAndSet获得的值会与原来的相等，第二个线程获得的值不会原来的值相等
            //以此确保锁的唯一性
            String old2 = redisTemplate.opsForValue().getAndSet("key","带时间戳的值");
            if(old2!=null&&old2==old){
                return true;
            }

        }
        return false;
    }
	
	问题：
	1. 由于是客户端自己生成过期时间，所以需要强制要求分布式下每个客户端的时间必须同步。
	2. 当锁过期的时候，如果多个客户端同时执行jedis.getSet()方法，那么虽然最终只有一个客户端可以拿到锁，但是这个客户端的锁的过期时间可能被其他客户端覆盖。
	
	


	/**
     * redis锁的正确使用方式
     * @param key  redis锁的key
     * @param requestId 请求标识
     * @param timeout   超时时间
     * @return 是否获取成功
     */
    public Boolean lock(String key, String requestId, Long timeout) {
        if (stringRedisTemplate.opsForValue().setIfAbsent(key, requestId, timeout, TimeUnit.SECONDS)) {
            return true;
        }
        return false;
    }

   

   /**
     * redis锁的错误使用
     * @param key redis锁的key
     * @param requestId 请求标识
     * @param timeout  超时时间
     * @return 是否获取成功
     */
    public Boolean errorlock(String key, String requestId, Long timeout) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, requestId);
        if (flag) {
            if (stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS)) {
                return true;
            }
        }
        return false;
    }
	
   问题：
   1.setIfAbsent和expire非原子性，程序设置了锁后程序崩溃，没设置失效时间造成死锁
   



   /**
     * 解锁的错误方式1
     * @param key redis解锁的key
     * @param requestId 请求标识
     * @return void
     */
    public void errorunlock(String key,String requestId){
        if(stringRedisTemplate.opsForValue().get(key).equals(requestId)){
            //走到这步当前客户端锁突然失效，然后其他客户端将会拿到锁，此时删除将会造成锁的误删
            stringRedisTemplate.delete(key);
        }
    }
	
	问题：
	1.当走到if里面后当前客户端锁突然失效，然后其他客户端将会拿到锁，此时删除将会造成锁的误删

    
	
	
	
	/**
     * 正确的解锁,利用lua脚本的原子性解锁
     * @param key redis解锁的key
     * @param requestId 请求标识
     * @return void
     */
    public void unlock(String key,String requestId){
        //unlock.lua内容
       /* local value = redis.call('get',KEYS[1])
        if value == ARGV[1]
        then
        return redis.call('del',KEYS[1])
        else
        return 0
        end*/
        DefaultRedisScript<Long> getRedisScript =  new DefaultRedisScript<>();
        getRedisScript.setResultType(Long.class);
        getRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("luascript/unlock.lua")));
        List<String> keys = new ArrayList();
        keys.add(key);
        String[] args ={requestId};
        Long result=stringRedisTemplate.execute(getRedisScript, keys, args);
    }
	
	
	
	
	还需要考虑的问题：
	
	如果加锁和解锁之间的业务非常耗时，那么就可能存在：
	线程一拿到锁之后执行业务
	还没执行完锁就超时过期了
	线程二此时拿到锁乘虚而入，开始执行业务...
	redis 分布式锁在死锁和超时问题之间做出的妥协，没办法完全避免，但是需要业务在使用时，衡量加锁的粒度及过期时间。
