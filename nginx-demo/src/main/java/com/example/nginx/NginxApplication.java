package com.example.nginx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class NginxApplication {

	public static void main(String[] args) {
		SpringApplication.run(NginxApplication.class, args);
	}

	@RequestMapping(value = "/test",method = RequestMethod.GET)
	public String test(){
		return "Ok2";
	}
}
